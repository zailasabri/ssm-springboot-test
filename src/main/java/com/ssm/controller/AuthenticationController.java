package com.ssm.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ssm.model.RefCountry;
import com.ssm.model.impl.RefCountryImpl;
import com.ssm.model.impl.RefStateImpl;
import com.ssm.service.impl.ReferenceService;

@Controller
public class AuthenticationController {
	
	@Autowired
	private ReferenceService refService;
			
	@RequestMapping(value={"", "/"}, method=RequestMethod.GET)
	public String index(ModelMap model){
		return "index";
	}
	
	@RequestMapping(value={"/login"}, method=RequestMethod.GET)
	public String login(ModelMap model){
		return "login";
	}
	
	@RequestMapping(value="/loginfailed", method=RequestMethod.GET)
	public String loginFailed(ModelMap model){
		model.addAttribute("error", "true");
		return "login";
	}

	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logout(ModelMap model) {
		return "login";
	}
	
	
	@RequestMapping(value="/userregistration", method = RequestMethod.GET)
	public String userRegistration(ModelMap model) {
		
		RefCountry refCountry = new RefCountryImpl();
		refCountry.setCountryId((long) 1);
		refCountry = refService.findById(refCountry);
		
		List<RefStateImpl> refState = new ArrayList<RefStateImpl>();
		refState = (List<RefStateImpl>) refService.listAllState();
		
		System.out.println("country name = " + refCountry.getCountryName());
		
		for(RefStateImpl state : refState)
			System.out.println("state name = " + state.getStateName());
		
		return "user/userRegistration";
	}
	
		
	/*
	@RequestMapping(value="/loginfailed", method=RequestMethod.GET)
	public String loginFailed(ModelMap model){
		model.addAttribute("error", "true");
		return "login";
	}
	
	@RequestMapping(value="/error", method=RequestMethod.GET)
	public String error(ModelMap model){
		model.addAttribute("error", "true");
		return "login";
	}

	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logout(ModelMap model) {
		return "login";
	}
*/
	@RequestMapping(value="/error", method=RequestMethod.GET)
	public String error(ModelMap model){
		model.addAttribute("error", "true");
		return "error";
	}
}
