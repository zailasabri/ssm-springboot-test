package com.ssm.model;

import java.sql.Date;

public interface AdmEmployee {

	Long getEmployeeId();

	void setEmployeeId(Long employeeId);

	String getEmployeeNumber();

	void setEmployeeNumber(String employeeNumber);

	AdmPerson getAdmPersonImpl();

	void setAdmPersonImpl(AdmPerson admPersonImpl);

	Date getStartDate();

	void setStartDate(Date startDate);

	AdmOrganizationUnit getAdmOrganizationUnitImpl();

	void setAdmOrganizationUnitImpl(AdmOrganizationUnit admOrganizationUnitImpl);

}