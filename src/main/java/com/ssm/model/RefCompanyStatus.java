package com.ssm.model;

public interface RefCompanyStatus {

	Long getCompanyStatusId();

	void setCompanyStatusId(Long companyStatusId);

	String getCompanyStatusCode();

	void setCompanyStatusCode(String companyStatusCode);

	String getCompanyStatus();

	void setCompanyStatus(String companyStatus);

}