package com.ssm.model;

public interface AdmAddress {

	Long getAddressId();

	void setAddressId(Long addressId);

	String getAddress1();

	void setAddress1(String address1);

	String getAddress2();

	void setAddress2(String address2);

	String getAddress3();

	void setAddress3(String address3);

	Long getCountryId();

	void setCountryId(Long countryId);

	String getCountryName();

	void setCountryName(String countryName);

	Long getStateId();

	void setStateId(Long stateId);

	String getStateName();

	void setStateName(String stateName);

	Long getAreaId();

	void setAreaId(Long areaId);

	String getAreaName();

	void setAreaName(String areaName);

	Long getPostcode();

	void setPostcode(Long postcode);

}