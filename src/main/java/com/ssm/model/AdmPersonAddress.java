package com.ssm.model;

public interface AdmPersonAddress {

	Long getPersonAddressId();

	void setPersonAddressId(Long personAddressId);

	Long getPersonTypeId();

	void setPersonTypeId(Long personTypeId);

	AdmAddress getAdmAddressImpl();

	void setAdmAddressImpl(AdmAddress admAddressImpl);

}