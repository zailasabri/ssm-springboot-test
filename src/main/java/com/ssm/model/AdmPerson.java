package com.ssm.model;

import java.sql.Date;

public interface AdmPerson {

	Long getPersonId();

	void setPersonId(Long personId);

	String getIdentityNumber();

	void setIdentityNumber(String identityNumber);

	Long getIdentityTypeId();

	void setIdentityTypeId(Long identityTypeId);

	String getPersonName();

	void setPersonName(String personName);

	Date getBirthDate();

	void setBirthDate(Date birthDate);

	Long getGenderId();

	void setGenderId(Long genderId);

	Long getRaceId();

	void setRaceId(Long raceId);

	Long getNationalityId();

	void setNationalityId(Long nationalityId);

	AdmPhoto getAdmPhotoImpl();

	void setAdmPhotoImpl(AdmPhoto admPhotoImpl);

}