package com.ssm.model;

import javax.persistence.Entity;

public interface RefCountry {

	Long getCountryId();

	void setCountryId(Long countryId);

	String getCountryCode();

	void setCountryCode(String countryCode);

	String getCountryName();

	void setCountryName(String countryName);

}