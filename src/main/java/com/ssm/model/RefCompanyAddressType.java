package com.ssm.model;

public interface RefCompanyAddressType {

	Long getCompanyAddressTypeId();

	void setCompanyAddressTypeId(Long companyAddressTypeId);

	String getCompanyAddressTypeCode();

	void setCompanyAddressTypeCode(String companyAddressTypeCode);

	String getCompanyAddressType();

	void setCompanyAddressType(String companyAddressType);

}