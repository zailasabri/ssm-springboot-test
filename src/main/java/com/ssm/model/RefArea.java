package com.ssm.model;

public interface RefArea {

	Long getAreaId();

	void setAreaId(Long areaId);

	RefState getRefState();

	void setRefState(RefState refState);

	String getAreaCode();

	void setAreaCode(String areaCode);

	String getAreaName();

	void setAreaName(String areaName);

}