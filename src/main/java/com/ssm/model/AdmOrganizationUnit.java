package com.ssm.model;

public interface AdmOrganizationUnit {

	Long getOrganizationUnitId();

	void setOrganizationUnitId(Long organizationUnitId);

	String getOrganizationUnitCode();

	void setOrganizationUnitCode(String organizationUnitCode);

	String getOrganizationUnitName();

	void setOrganizationUnitName(String organizationUnitName);

	Long getOrganizationUnitTypeId();

	void setOrganizationUnitTypeId(Long organizationUnitTypeId);

	AdmAddress getAdmAddressImpl();

	void setAdmAddressImpl(AdmAddress admAddressImpl);

}