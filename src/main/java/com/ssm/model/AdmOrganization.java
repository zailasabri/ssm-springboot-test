package com.ssm.model;

public interface AdmOrganization {

	Long getOrganizationId();

	void setOrganizationId(Long organizationId);

	String getOrganizationCode();

	void setOrganizationCode(String organizationCode);

	String getOrganizationName();

	void setOrganizationName(String organizationName);

}