package com.ssm.model;

public interface AdmPhoto {

	Long getPhotoId();

	void setPhotoId(Long photoId);

	byte[] getFileimage();

	void setFileimage(byte[] fileimage);

	String getFileext();

	void setFileext(String fileext);

	String getFilename();

	void setFilename(String filename);

}