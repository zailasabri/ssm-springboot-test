package com.ssm.model;

public interface AdmOrganizationAddress {

	Long getOrganizationAddressId();

	void setOrganizationAddressId(Long organizationAddressId);

	AdmOrganization getAdmOrganizationImpl();

	void setAdmOrganizationImpl(AdmOrganization admOrganizationImpl);

	AdmAddress getAdmAddressImpl();

	void setAdmAddressImpl(AdmAddress admAddressImpl);

}