package com.ssm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ssm.model.RefCountry;

@Repository
public class RefCountryDaoImpl extends CrudDaoImpl<RefCountry>{


	@Override
	public RefCountry findById(RefCountry t) throws HibernateException {
		return (RefCountry) sessionFactory.getCurrentSession().get(t.getClass(), t.getCountryId());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RefCountry> getAll(RefCountry t) throws HibernateException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(t.getClass());
		if (t.getCountryCode() != null) {
			criteria.add(Restrictions.eq("countryCode", t.getCountryCode()));
		}

		if (t.getCountryName() != null) {
			criteria.add(Restrictions.like("countryName", "%" + t.getCountryName() + "%",
					MatchMode.ANYWHERE));
		}	
		
		return criteria.list();
	}
}
