package com.ssm.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ssm.dao.CrudDao;
import com.ssm.model.CrudModel;

@Transactional
@SuppressWarnings("unchecked")
@Component
public class CrudDaoImpl<T> implements CrudDao<T> {
	
	protected SessionFactory sessionFactory;
	
	
	@Override
	public void add(T t) throws HibernateException {
		sessionFactory.getCurrentSession().save(t);		
	}
	
	@Override
	public void update(T t) throws HibernateException {
		sessionFactory.getCurrentSession().saveOrUpdate(t);
	}
	
	@Override
	public void delete(T t) throws HibernateException {
		sessionFactory.getCurrentSession().delete(t);
	}
	
	@Override
	public T findById(T t) throws HibernateException {
		// convert t to CrudModel to get the id
		CrudModel crudModel = (CrudModel) t; 
		
		
		return (T) sessionFactory.getCurrentSession().get(t.getClass(), crudModel.getIdCrud());
	}

	@Override
	public List<T> getAllByLimit(T t, String paramSort, String orderSort,
			int start, int limit) throws HibernateException {
			
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(t.getClass());
		if (orderSort != null && orderSort.equalsIgnoreCase("ASC")) {
			if (paramSort != null && !paramSort.isEmpty()) {
				criteria.addOrder(Order.asc(paramSort));
			}
		} else {
			if (paramSort != null && !paramSort.isEmpty()) {
				criteria.addOrder(Order.desc(paramSort));
			}
		}
		
		//add limit
		criteria.setFirstResult(start);
		criteria.setMaxResults(limit);
		
		return criteria.list();
	}
	
	@Override
	public int countAll(T t) throws HibernateException {
		return (int) (long) sessionFactory.getCurrentSession()
				.createCriteria(t.getClass())
				.setProjection(Projections.rowCount()).uniqueResult();
	}
	
	@Override
	public List<T> getAll(T t) throws HibernateException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				t.getClass());
		return criteria.list();
	}

}
