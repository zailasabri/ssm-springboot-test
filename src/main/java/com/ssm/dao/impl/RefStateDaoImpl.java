package com.ssm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.ssm.model.RefState;

public class RefStateDaoImpl extends CrudDaoImpl<RefState>{

	
	@SuppressWarnings("unchecked")
	@Override
	public List<RefState> getAll(RefState t) throws HibernateException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(t.getClass());
		if (t.getRefCountry() != null) {
			criteria.add(Restrictions.eq("refCountry", t.getRefCountry()));
		}
		
		return criteria.list();
	}
}
