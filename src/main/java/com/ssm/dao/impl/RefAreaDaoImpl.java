package com.ssm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.ssm.model.RefArea;

public class RefAreaDaoImpl extends CrudDaoImpl<RefArea>{


	@SuppressWarnings("unchecked")
	@Override
	public List<RefArea> getAll(RefArea t) throws HibernateException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(t.getClass());
		if (t.getRefState() != null) {
			criteria.add(Restrictions.eq("refState", t.getRefState()));
		}
		
		return criteria.list();
	}
}
