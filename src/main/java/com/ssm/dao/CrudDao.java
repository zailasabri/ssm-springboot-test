package com.ssm.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

@Repository
public interface CrudDao<T> {

	void add(T t) throws HibernateException;

	void update(T t) throws HibernateException;

	void delete(T t) throws HibernateException;

	T findById(T t) throws HibernateException;

	List<T> getAllByLimit(T t, String paramSort, String orderSort, int start, int limit) throws HibernateException;

	int countAll(T t) throws HibernateException;

	List<T> getAll(T t) throws HibernateException;

}