package com.ssm.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ssm.model.impl.RefStateImpl;

@Repository
public interface RefStateDao extends CrudRepository<RefStateImpl, Integer>{
	
}
